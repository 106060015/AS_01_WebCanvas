var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var detect;
//redoundo
var rudoArray = new Array();
var ru_cnt;
var blank=ctx.getImageData(0,0,1000,600);
rudoArray.push(blank);
function ru(){
  var imgData=ctx.getImageData(0,0,1000,600);
  if((ru_cnt < rudoArray.length-1) && (rudoArray.length != 1)){
    rudoArray.length = ru_cnt; 
  }
  rudoArray.push(imgData);
  ru_cnt = rudoArray.length;
}
//redoundo
//draw shapes
var begin_x;
var begin_y;
var fill_mode = 1;
//draw shapes

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.putImageData(rudoArray[ru_cnt-1],0,0);
  if (detect == "pen") {
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
  } 
  if (detect == "cir") {
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
    ctx.beginPath(); 
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
    ctx.arc(begin_x, begin_y, Math.sqrt((begin_x-mousePos.x)*(begin_x-mousePos.x) + (begin_y-mousePos.y)*(begin_y-mousePos.y)), 0, 2 * Math.PI);
    ctx.closePath();
    if(fill_mode){
      ctx.fillStyle=ctx.strokeStyle;
      ctx.fill();
    }
    ctx.stroke();
  }
  if (detect == "rec") {
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
    ctx.beginPath(); 
    ctx.lineTo(mousePos.x, begin_y);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.lineTo(begin_x, mousePos.y);
    ctx.lineTo(begin_x, begin_y);
    ctx.closePath();
    if(fill_mode){
      ctx.fillStyle = ctx.strokeStyle;
      ctx.fill();
    }
    ctx.stroke();  
  }
  if (detect == "tri") {
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
    ctx.beginPath();  
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.lineTo(2*begin_x-mousePos.x, mousePos.y);
    ctx.lineTo(begin_x, begin_y);
    ctx.closePath();
    if(fill_mode){
      ctx.fillStyle=ctx.strokeStyle;
      ctx.fill();
    }
    ctx.stroke();
    
  }
}

canvas.addEventListener('mousedown', function (evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  if(detect == "pen" || detect == "rec" || detect == "tri"){
    ctx.moveTo(mousePos.x, mousePos.y);
  }
  begin_x = mousePos.x;
  begin_y = mousePos.y;
  evt.preventDefault();
  canvas.addEventListener('mousemove', mouseMove, false);
}, false);

canvas.addEventListener('mouseup', function () {
  canvas.removeEventListener('mousemove', mouseMove, false);
  ru();
}, false);

document.getElementById('reset').addEventListener('click', function () {
  detect = 0;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ru_cnt = 1;
}, false);

document.getElementById('pen').addEventListener('click', function () {
  detect = "pen";
}, false);

document.getElementById('eraser').addEventListener('click', function () {
  detect = "pen";
  ctx.strokeStyle = colors[8];
}, false);

document.getElementById('cir').addEventListener('click', function () {
  detect = "cir";
}, false);
document.getElementById('rec').addEventListener('click', function () {
  detect = "rec";
}, false);
document.getElementById('tri').addEventListener('click', function () {
  detect = "tri";
}, false);

document.getElementById('text').addEventListener('click', function (evt) {
  detect = 7;
  
  document.addEventListener('click', function (evt) {
  if(detect == 7){  
    var mousePos = getMousePos(canvas, evt);
    ctx.fillStyle = ctx.strokeStyle;
    var words = document.getElementById('123').value;
   if(mousePos.x>0 && mousePos.x<1000 && mousePos.y>0 && mousePos.y<600){
      ctx.fillText(words,mousePos.x, mousePos.y); 
      ru();
    }
    //detect = 0;   
    } 
  });
}, false);


document.getElementById('download').addEventListener('click', function () {
  image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
  var link = document.createElement('a');
  link.download = "my-image.png";
  link.href = image;
  link.click();
}, false);

document.getElementById('upload').addEventListener('click', function () {
  var inputObj = document.createElement('input');
  inputObj.addEventListener('change', readFile, false);
  inputObj.type = 'file';
  inputObj.accept = 'image/*';
  inputObj.click();
  function readFile() {
    var file = this.files[0];//获取input输入的图片
    var reader = new FileReader();
    reader.readAsDataURL(file);//转化成base64数据类型
    reader.onload = function (e) {
      drawToCanvas(this.result);
    }
  }
  function drawToCanvas(imgData) {
    var img = new Image;
    img.src = imgData;
    img.onload = function () {//必须onload之后再画
    ctx.drawImage(img, 125, 75, 875, 525);
    ru(); 
  }
  }
  
}, false);

document.getElementById('redo').addEventListener('click', function () {
  if (rudoArray.length > ru_cnt ) {
    ru_cnt += 1;
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
    console.log(123);
  }
}, false);

document.getElementById('undo').addEventListener('click', function () {
  if (ru_cnt > 1 ) {
    ru_cnt -= 1;
    ctx.putImageData(rudoArray[ru_cnt-1],0,0);
  }
}, false);

var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white'];
var size = [1, 3, 5, 10, 15, 20];
var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];


function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function () {
    ctx.strokeStyle = colors[i];
  }, false);
}

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function () {
    ctx.lineWidth = size[i];
  }, false);
}

for (var i = 0; i < colors.length; i++) {
  listener(i);
}
for (var i = 0; i < size.length; i++) {
  fontSizes(i);
}
$(text_fontType).change(function(){
  ctx.font = text_fontSize.value + "px " + text_fontType.value;
})
$(text_fontSize).change(function(){
  ctx.font = text_fontSize.value + "px " + text_fontType.value;
})